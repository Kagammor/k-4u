<!DOCTYPE html>
<html>
	<head>
		<title>K-4U</title>
		
		<link href="./assets/css/style.css" rel="stylesheet" type="text/css">
	</head>
	
	<body>
		<div id="header_backdrop"></div>
	
		<div id="container">
			<header>
				<a href="/projects/k-4u"><img src="./assets/img/logo.png" id="logo"></a>
				
				<nav>
					<ul>
						<li><a href="#">nieuws</a></li>
						<li><a href="#">portfolio</a></li>
						<li><a href="#">over</a></li>
						<li><a href="#" id="contact">contact</a></li>
					</ul>
				</nav>
				
				<section id="breadcrumbs">
					home > bread > crumbs
				</section>
				
				<img src="./assets/img/block.png" id="block">
			</header>
			
			<section id="lightbox">
				<img src="./assets/img/lightbox/mobile.jpg" id="lightbox_background">
			</section>
			
			<section id="content">
				<div id="lightbox_info">
					<h1>Mobiele applicaties</h1>
					
					<p>K-4U ontwikkelt ook voor Android,<br>
					h&eacute;t onovertroffen mobiele platform van dit moment, en de toekomst.</p>
				</div>
				
				<section id="information">
					<h1>Een eland beet ooit eens mijn zus...</h1>
					
					<p id="panel1">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed nec lectus ut augue venenatis laoreet non gravida leo. Mauris imperdiet luctus magna. Aliquam a lectus vitae mauris laoreet eleifend. Fusce eget fringilla risus. Maecenas sodales mattis tellus nec lobortis. Vestibulum suscipit volutpat condimentum. Nam id nulla lectus. Integer vitae aliquet purus. Cras consectetur facilisis erat, sit amet fermentum dui dignissim a. Aliquam ac velit et magna consequat commodo.
					</p>
					
					<p id="panel2">
						Curabitur at varius velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Cras suscipit, ligula vel convallis dignissim, elit libero egestas ante, vel lacinia tortor ligula et nulla. Pellentesque placerat interdum tellus, eu facilisis enim pellentesque sit amet. Suspendisse augue est, aliquet vel congue id, adipiscing nec dolor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis tortor lorem, ultricies vel cursus ac, malesuada at neque.
					</p>
				</section>
			</section>
			
		</div>
	</body>
</html>
